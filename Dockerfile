## ARGUMENTOS:
##   GANCIO_VERSION  tag/rama desde la que se construye gancio (defecto: master)
##   GANCIO_UID      uid (identificador numerico) de la usuaria que ejecutara gancio (defecto: 110)


## Etapa 1: COMPILACION de Gancio
##
## - Descarga codigo de Framagit.org
## - Compila la libreria Gancio
## - Dependencias: nodejs + yarn
##
FROM registry.sindominio.net/debian

RUN apt-get update && \
    apt-get install -y --no-install-recommends curl gnupg2 ca-certificates git

# Instalar Node Latest
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
    apt-get install -y nodejs

WORKDIR /opt

ARG GANCIO_VERSION=master

RUN echo "deb http://deb.debian.org/debian buster-backports main" >> /etc/apt/sources.list && \
    echo "deb https://dl.yarnpkg.com/debian/ stable main" > /etc/apt/sources.list.d/yarn.list && \
    curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg |  apt-key add -

# Instalar Yarn
RUN apt-get update && \
	  apt-get install -y --no-install-recommends yarn

# Clonar codigo de Gancio
RUN git clone https://framagit.org/les/gancio.git && \
    cd gancio && \
    git checkout $GANCIO_VERSION

# Aplicar parches
WORKDIR /opt/gancio
COPY patches patches
RUN git apply patches/*.patch

# Instalar dependencias de desarrollo, compilar y empaquetar
RUN yarn install && \
    yarn build && \
    yarn pack --filename=gancio.tgz



## 2da Etapa: EJECUCION de Gancio
##
## - Usa la libreria Gancio compilada antes
## - Instala todas las dependencias de produccion
## - Crea un link en /usr/local/bin/gancio
## - Dependencias: nodejs
##
FROM registry.sindominio.net/debian

RUN apt-get update && \
  	apt-get install -y --no-install-recommends curl gnupg2 ca-certificates git

# Instalar Node Latest
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash - && \
    apt-get install -y nodejs

# Copiar y descomprimir el servicio de Gancio compilado en 1ra Etapa
COPY --from=0 /opt/gancio/gancio.tgz ./

RUN tar zxf gancio.tgz && \
    mv package gancio && \
    rm gancio.tgz 

WORKDIR /gancio

# Instalar solo las dependencias de produccion y crear symlink para ejecucion
RUN npm install --production && \
    ln -s /gancio/server/cli.js /usr/local/bin/gancio

# Crear usuaria gancio para ejecutar el servicio
ARG GANCIO_UID=110

RUN useradd -u $GANCIO_UID -g nogroup gancio

# Compiar el script de entrada del repo y ejecutar
ADD entrypoint.sh /

RUN chmod 750 /entrypoint.sh

ENTRYPOINT [ "/bin/sh", "/entrypoint.sh" ]
