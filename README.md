# GANCIO for SINDOMINIO

## Dependencias
El servidor que vaya a ejecutar esta version dockerizada de Gancio necesita contar
con un demonio de Docker, asi como con el binario `docker-compose`.

Estas dos dependencias suelen instalarse conjuntamente, como un paquete de las
diferentes distribuciones linux, la [documentacion oficial de Docker][0] nos ayuda a
hacerlo

[0]: https://docs.docker.com/engine/install/


## Inicio Rapido
Si simplemente quieres poner a andar una instancia de Gancio, puedes seguir
los pasos en las secciones [instalacion manual](#instalacion-manual),
[configuracion](#configuracion-del-servicio) y [ejecucion](#ejecucion-de-gancio)

Resumidos quedan asi:
```bash
git clone https://git.sindominio.net/gancio/docker-gancio
cd docker-gancio
mkdir data
touch data/config.json data/db.sqlite
cp .env.sample .env
docker-compose build
docker-compose run --rm gancio "gancio setup --docker --db=sqlite"
docker-compose up -d
```

Si has usado las configuraciones por defecto, Gancio deberia estar disponible
en <http://localhost:13120>


## Instrucciones originales de instalacion
<https://gancio.org/install/docker>


## Construccion del docker para Gancio
El original se basa en la imagen de node disponible en DockerHub

```
FROM node:latest
...
```

Nosotros elegimos funcionar a través de nuestro Debian en nuestro registry,
asi que adaptamos el Dockerfile al estilo la [guía de instalación de GNU/Debian][1]

[1]: https://gancio.org/install/debian


```
FROM registry.sindominio.net/debian
...
```

Ejecutamos un poroceso de construccion del docker de 2 etapas. En la __1ra
etapa__, clonamos el codigo del repositorio de gancio y compilamos el servicio.
En la __2da etapa__ utilizamos el servicio compilado en la etapa anterior, y
lo instalamos en un docker basado en el debian de SinDominio, e instalamos las
minimas dependencias necesarias para ejecutar el proyecto.


### Nuevo punto de entrada al docker
Agregamos un script de entrada, `entrypoint.sh`, que se ocupa de
validar la existencia de los datos/variables necesarias para poder ejecutar el
servicio, gestiona la propiedad de los archivos de datos de gancio, e inicia
el servicio como la usuaria `gancio`


### Ejecucion desempoderada
Por razones de seguridad, evitamos ejecutar servicios dentro de los dockers
como usuaria `root`. En este caso, ejecutaremos el servicio como usuaria
`gancio` perteneciente al grupo `nogroup`


### Parametros disponibles en la construccion del docker
`docker-compose`, al igual que `docker`,  nos permite pasar variables de entorno
al proceso de construccion del contenedor, utilizando la opcion
`--build-arg MI_VAR=valor`

Nuestro nuevo proceso de construccion toma los siguientes parametros, ambos
opcionales:
* `GANCIO_VERSION`, contiene el nombre de la rama o tag de git que queremos
  utilizar _(valor por defecto: `master`)_
* `GANCIO_UID`, contiene el indicador numerico asociada a la usuaria `gancio`
  que ejecutara el servicio dentro del docker _(valor por defecto: `110`)_


## Instalacion Manual

Si estamos instalando Gancio manualmente, podemos seguir los pasos de la
docuemntacion de Gancio. Este proceso requere intervencion de la usuaria que
debe responder a un cuestionario durante la etapa de Setup.

Primero, descargamos este repo
```
git clone https://git.sindominio.net/gancio/docker-gancio
```

Accedemos a la carpeta creada
```
cd docker-gancio
```

Creamos los ficheros básicos para la instalacion y configuracion del servicio:
```
mkdir data
touch data/config.json data/db.sqlite
cp .env.sample .env
```

Luego a buildear
```
docker-compose build
```

Y por ultimo, respondems a las preguntas del instalador
```
docker-compose run --rm gancio "gancio setup --docker --db=sqlite"
```


## Instalacion Automatica

En este caso, nos interesa desplegar Gancio sin intervencion de una usuaria.
Queremos:
* evitar el questionario de setup
* proveer datos necesarios via `config.json` o variables de entorno

Necesitamos, entonces, un archivo `config.json` con la configuracion de la
instancia. Nuestro repositorio incluye un archivo `config.json.sample` a modo
de ejemplo. Te recomendamos copiar este archivo dentro del directorio de datos
que vayas a utilizar para Gancio y editarlo con los datos de tu instancia
```bash
git clone https://git.sindominio.net/gancio/docker-gancio
cd docker-gancio
mkdir data
cp config.json.sample data/config.json
```

Al editar el archvio, los datos que nos interesa cambiar en este archivo son
- `title`, nombre de nuestra instancia de Gancio
- `baseurl`, url publica de este servicio
- `secret`, sal para los datos encryptados en la bbdd. puedes generar uno
  nuevo con `cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w ${1:-24} | head -n 1`
- `admin_email`, email de la cuenta de administradora (se usa para acceder al
  gestionar el servicio)
- `admin_pass`, clave de acceso para la cuenta de administradora. _SE ELIMINA
  AUTOMATICAMENTE DEL FICHERO AL CREAR LA CUENTA_
- `smtp`, si se desea enviar correos, modificar a los datos de vuestro
  servidor de SMPT

No modifiques los siguientes datos/secciones, ya que modificarian la forma en
que Gancio funciona dentro del docker y podria romper el servicio si no estas
segura de lo que haces ;)
- `server`
- `upload_path`
- `db`


### Cuenta de administradora
Esta cuenta solo se creara en el caso de iniciar una nueva instancia de
Gancio, sin datos.

Si estamos re-iniciando una instancia existente, estos datos seran ignorados
y se deberan conocer los datos de la cuenta de administradora originales.

Entonces, en una instancia nueva iniciada sin datos, la administradora podra
loguearse utilizando el dato `admin_email` y `admin_pass` informado en el
`config.json`


### Autenticacion para SMTP
Si tu servidor de correos necesita de autenticacion, debemos persistir esta
informacion en la seccion `smtp` del fichero `config.json`
```json
  ...
  "smtp": {
    "host": "localhost",
    "port": 25,
    "auth": {
      "user": "<smtp_user>",
      "pass": "<smtp_password>"
    }
  },
  ...
```


## Configuracion del servicio
Dado que utilizamos `docker-compose` para ejecutar Gancio, aprovecharemos la
posibilidad de definir nuestras variables de entorno en un archivo `.env` que
deberemos mantener en la misma carpeta que nuestro `docker-compose.yml`

Este repositorio contiene un `.env.sample` a modo de ejemplo. Si te valen los
valores por defecto, puedes copiar el archivo `.env.sample` y llamarlo `.env`
```bash
cp .env.sample .env
```

### Variables de Despliegue (necesarias en todos los casos)
- `GANCIO_DATA_PATH`, ruta en el host que contiene los datos de Gancio
  _(valor por defecto `./data` = subcarpeta `data` en la misma carpeta que
  `docker-compose.yml`)_
- `GANCIO_PORT`, puerto del host en el que se expondra el servicio de Gancio
  _(valor por defecto `13120`)_



## Ejecucion de Gancio
Llegado este punto, podemos iniciar nuestra intancia
```
docker-compose up -d
```

Y a disfrutar de tu servicio de Gancio \o/
