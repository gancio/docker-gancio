#!/bin/bash

# verifica que exista un archivo de configuracion
[ -f /opt/gancio/config.json ] || { echo 'falta el archivo de configuracion config.json'; exit 1; }

# si no existe, crea la base de datos sqlite
[ -f /opt/gancio/db.sqlite ] || touch /opt/gancio/db.sqlite

# recupera permisos a nombre de la usuaria gancio
chown -R gancio:nogroup /opt/gancio

# ejecuta el Command pasado al docker, como usuaria gancio
su gancio --command="$@"
